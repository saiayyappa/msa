package com.practice.order.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.discovery.EurekaClient;
import com.practice.order.model.Orders;
import com.practice.order.service.OrderService;

@RestController
public class OrderController {

	private final Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	UserProxy proxy;

	@Autowired
	OrderService orderService;

	@Autowired
	@Lazy
	private EurekaClient eurekaClient;

	@Value("${spring.application.name}")
	private String appName;

	@Value("${server.port}")
	private String portNumber;

	@RequestMapping("/ordergreet")
	public String greeting() {
		logger.info(portNumber);
		return String.format("Response from '%s' service on port number '%s'",
				eurekaClient.getApplication(appName).getName(), portNumber);
	}

	@GetMapping(path = "/orders")
	public List<Orders> getOrders() {
		logger.info("Get all orders...");
		return orderService.getAllOrders();
	}

	@GetMapping(path = "/order/{orderId}")
	public Optional<Orders> getOrderById(@PathVariable Long orderId) {
		logger.info("Get user with id " + orderId);
		return orderService.getOrderById(orderId);
	}

	// Whenever a new order is created send order details to user service
	@RequestMapping(path = "/order")
	public String createOrder(@RequestBody Orders order) {
		logger.info("Creating Order");
		Orders o = orderService.createOrder(order.getUserId());
		proxy.sendOrderToUser(o.getId(), o.getUserId()); // Send order details to user service
		return "Order for userId " + order.getUserId() + " successfully created.";
	}

	@DeleteMapping("/order/{userId}")
	public String deleteOrderForUserId(@PathVariable(value = "userId") Long userId) {
		logger.info("Deleted Order for userId: " + userId);
		orderService.deleteOrderForUserId(userId);
		return "Order deleted for userId: " + userId;
	}
}
