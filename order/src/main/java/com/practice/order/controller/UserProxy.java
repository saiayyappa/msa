package com.practice.order.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "user")
public interface UserProxy {

	@RequestMapping("/usergreet")
	public String greeting();

	@RequestMapping("/order/{orderId}/{userId}")
	public void sendOrderToUser(@PathVariable(value = "orderId") Long orderId,
			@PathVariable(value = "userId") Long userId);

}
