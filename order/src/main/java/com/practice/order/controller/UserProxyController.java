package com.practice.order.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.order.service.OrderService;

@RefreshScope
@RestController
public class UserProxyController {

	private final Logger logger = LoggerFactory.getLogger(UserProxyController.class);

	@Autowired
	UserProxy proxy;

	@Autowired
	OrderService orderService;

	@RequestMapping("/check1")
	public String findAllOrders() {
		logger.info("Success User microservice hit from Order microservice");
		return proxy.greeting();
	}

}
