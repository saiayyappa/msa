package com.practice.order.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;

import com.practice.order.model.Orders;

public interface OrderRepo extends CrudRepository<Orders, Long> {

	@Async
	public String deleteByUserId(Long userId);

}
