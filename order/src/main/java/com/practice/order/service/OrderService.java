package com.practice.order.service;

import java.util.List;
import java.util.Optional;

import com.practice.order.model.Orders;

public interface OrderService {

	public List<Orders> getAllOrders();

	public Orders createOrder(Long userId);

	public Optional<Orders> getOrderById(Long orderId);
	
	public String deleteOrderForUserId(Long userId);

}
