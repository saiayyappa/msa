package com.practice.order.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.practice.order.model.Orders;
import com.practice.order.repo.OrderRepo;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepo orderRepo;

	@Override
	public List<Orders> getAllOrders() {
		return (List<Orders>) orderRepo.findAll();
	}

	@Override
	public Orders createOrder(Long userId) {
		return orderRepo.save(new Orders(userId));
	}

	@Override
	public Optional<Orders> getOrderById(Long orderId) {
		return (Optional<Orders>) orderRepo.findById(orderId);
	}

	@Override
	public String deleteOrderForUserId(Long userId) {
		orderRepo.deleteByUserId(userId);
		return "Deleted successfully";
	}

}
