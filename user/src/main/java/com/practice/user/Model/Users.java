package com.practice.user.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String username;

	// @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade =
	// CascadeType.ALL)
	// private Set<Orders> orders;

	@Column
	private String status;

	public Users() {

	}

	public Users(String username) {
		super();
		this.username = username;
	}

	public Users(Long userId, String username) {
		super();
		this.id = userId;
		this.username = username;
	}

	public Users(Long userId, String username, String status) {
		super();
		this.id = userId;
		this.username = username;
		this.status = "CREATED";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", username=" + username + ", status=" + status + "]";
	}

}
