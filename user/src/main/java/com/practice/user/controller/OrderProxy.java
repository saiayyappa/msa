package com.practice.user.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "order")
public interface OrderProxy {

	@RequestMapping("/ordergreet")
	public String greeting();
	
	@DeleteMapping("/order/{userId}")
	public String deleteOrderForUserId(@PathVariable(value="userId") Long userId);

}
