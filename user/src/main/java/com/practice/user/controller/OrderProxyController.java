package com.practice.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class OrderProxyController {

	@Autowired
	OrderProxy proxy;

	@RequestMapping("/check")
	public String findAllOrders() {
		System.out.println("Success Order microservice hit from User microservice");
		return proxy.greeting();
	}
	
}
