package com.practice.user.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.discovery.EurekaClient;
import com.practice.user.Model.Orders;
import com.practice.user.Model.Users;
import com.practice.user.repo.OrderRepo;
import com.practice.user.service.UserService;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {

	@Autowired
	OrderProxy proxy;

	@Autowired
	UserService userService;

	@Autowired
	@Lazy
	private EurekaClient eurekaClient;

	@Autowired
	OrderRepo orderRepo;

	private final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Value("${spring.application.name}")
	private String appName;

	@Value("${server.port}")
	private String portNumber;

	@RequestMapping("/usergreet")
	public String greeting() {
		logger.info(portNumber);
		return String.format("Response from '%s' service on port number '%s'",
				eurekaClient.getApplication(appName).getName(), portNumber);
	}

	@GetMapping(path = "/users")
	public List<Users> getUsers() {
		logger.info("Get all Users...");
		return userService.getUsers();
	}

	@PostMapping(path = "/user")
	public String createUser(@RequestBody Users user) {
		logger.info("Creating User");
		user.setStatus("CREATED");
		userService.createUser(user);
		return "User " + user.getUsername() + " successfully created";
	}

	@GetMapping(path = "/user/{userId}")
	public Optional<Users> getUserById(@PathVariable Long userId) {
		logger.info("Get user with id " + userId);
		return userService.getUserById(userId);
	}

	@RequestMapping(path = "/order/{orderId}/{userId}")
	public void sendOrderToUser(@PathVariable(value = "orderId") Long orderId,
			@PathVariable(value = "userId") Long userId) {
		logger.info(orderId + " " + userId);
		orderRepo.save(new Orders(orderId, userId));
		// userWithOrdersRepo.save(new UserWithOrders(userId, orderId));
	}

	@PutMapping(path = "/user")
	public String deleteUser(@RequestBody Users user) {
		logger.info("Deleting userId " + user.getId());
		user.setUsername(userService.getUserById(user.getId()).get().getUsername());
		user.setStatus("DELETED");
		userService.deleteUser(user);
		proxy.deleteOrderForUserId(user.getId());
		return "Deleted the user " + user.getId() + " successfully";
	}
}
