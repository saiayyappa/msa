package com.practice.user.repo;

import org.springframework.data.repository.CrudRepository;

import com.practice.user.Model.Orders;

public interface OrderRepo extends CrudRepository<Orders, Long> {

}
