package com.practice.user.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.practice.user.Model.Users;

public interface UserRepo extends CrudRepository<Users, Long> {

	public List<Users> findByStatus(String status);
}
