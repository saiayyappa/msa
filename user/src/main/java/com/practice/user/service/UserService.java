package com.practice.user.service;

import java.util.List;
import java.util.Optional;

import com.practice.user.Model.Users;

public interface UserService {

	public List<Users> getUsers();

	public String createUser(Users user);

	public Optional<Users> getUserById(Long userId);

	public String deleteUser(Users user);

}
