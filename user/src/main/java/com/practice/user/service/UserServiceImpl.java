package com.practice.user.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.user.Model.Users;
import com.practice.user.repo.UserRepo;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepo userRepo;

	@Override
	public List<Users> getUsers() {
		return (List<Users>) userRepo.findByStatus("CREATED");
	}

	@Override
	public String createUser(Users user) {
		return userRepo.save(user).getUsername();
	}

	@Override
	public Optional<Users> getUserById(Long userId) {
		return (Optional<Users>) userRepo.findById(userId);
	}

	@Override
	public String deleteUser(Users user) {
		userRepo.save(user);
		return "Status updated :: DELETED";
	}

}
