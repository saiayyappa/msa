package com.practice.user;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.practice.user.Model.Users;
import com.practice.user.service.UserService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = UserApplication.class)
class UserApplicationTests {

	@Mock
	UserService mock;

	@Test
	public void whenUserNameIsProvided_thenUserIsCreated() {
//		UserService mock = Mockito.mock(UserService.class);
		Mockito.when(mock.createUser(new Users("sai"))).thenReturn("sai");
		String expectedUser = new Users("sai").getUsername();
		String data = mock.createUser(new Users("sai"));
		assertEquals(expectedUser, data);
	}

	@Test
	public void WhenUserIdIsProvided_thenUserIsFetched() {
//		UserService mock = Mockito.mock(UserService.class);
		Long userId = new Long("1");
		Users expectedUser = new Users(userId, "sai");
		Mockito.when(mock.getUserById(userId)).thenReturn(Optional.of(expectedUser));
		Optional<Users> data = mock.getUserById(userId);
		assertEquals(expectedUser, data.get());
	}
}
